package com.homework.hw2481;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Color;
import android.view.Menu;
import android.view.View;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
  //These are the functions that are called by the buttons
	public void setBackgroundRed(View view) {
		getWindow().getDecorView().setBackgroundColor(Color.RED);
	}
	public void setBackgroundBlue(View view) {
		getWindow().getDecorView().setBackgroundColor(Color.BLUE);
	}
	public void setBackgroundGreen(View view) {
		getWindow().getDecorView().setBackgroundColor(Color.GREEN);
	}

}
